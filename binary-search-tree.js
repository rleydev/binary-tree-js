class Node {
  constructor(value) {
    this.value = value
    this.left = null
    this.right = null
  }
}

class Tree {
  constructor() {
    this.root = null
  }

  insert(value) {
    const newNode = new Node(value)

    if (!this.root) {
      this.root = newNode
      return
    }

    let current = this.root

    while (true) {
      if (value < current.value) {
        if (!current.left) {
          current.left = newNode
          return
        }

        current = current.left
      } else {
        if (!current.right) {
          current.right = newNode
          return
        }

        current = current.right
      }
    }
  }

  *inorderTraversal(node = this.root) {
    if (!node) return

    yield* this.inorderTraversal(node.left)
    yield node.value
    yield* this.inorderTraversal(node.right)
  }
}

document.addEventListener("DOMContentLoaded", (e) => {
  const tree = new Tree()

  const canvas = document.getElementById("tree-canvas")
  const ctx = canvas.getContext("2d")

  document.addEventListener("keypress", (event) => {
    if (event.code === "Space") {
      const value = generateNumber()
      tree.insert(value)

      // Clear the canvas and redraw the tree
      ctx.clearRect(0, 0, canvas.width, canvas.height)
      drawTree(ctx, tree.root, canvas.width / 2, 50, 200)
    }
  })

  function generateNumber() {
    return Math.floor(Math.random() * 201) - 100
  }

  // Draw recursively
  function drawTree(ctx, node, x, y, spacing) {
    if (!node) return

    // current node
    ctx.beginPath()
    ctx.arc(x, y, 20, 0, 2 * Math.PI)
    ctx.stroke()
    ctx.fillText(node.value.toString(), x - 6, y + 6)

    // left subtrees
    if (node.left) {
      ctx.beginPath()
      ctx.moveTo(x, y + 20)
      ctx.lineTo(x - spacing, y + 50)
      ctx.stroke()
      drawTree(ctx, node.left, x - spacing, y + 50, spacing / 2)
    }
    // right subtrees
    if (node.right) {
      ctx.beginPath()
      ctx.moveTo(x, y + 20)
      ctx.lineTo(x + spacing, y + 50)
      ctx.stroke()
      drawTree(ctx, node.right, x + spacing, y + 50, spacing / 2)
    }
  }
})
